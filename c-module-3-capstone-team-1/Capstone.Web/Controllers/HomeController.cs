﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Capstone.Web.Models;
using Capstone.Web.DAL;
using Microsoft.AspNetCore.Http;
using Capstone.Web.Extensions;

namespace Capstone.Web.Controllers {
    public class HomeController : Controller {
        IParkDAL parkDAL;
        IWeatherDAL weatherDAL;

        public HomeController(IParkDAL parkDAL, IWeatherDAL weatherDAL) {
            this.parkDAL = parkDAL;
            this.weatherDAL = weatherDAL;
        }

        public IActionResult Index(ParkInfo parks) {
            parks.Results = parkDAL.GetAllParks();
            return View(parks);
        }

        public IActionResult Detail(ParkInfo park) {
            ParkInfo detailPark = parkDAL.GetPark(park.ParkCode);
            detailPark.Weather = weatherDAL.GetParkForecast(park.ParkCode);
            detailPark.TempCategories = GetTempCategory();
            if (detailPark.TempCategories == "Celsius") {
                detailPark.Weather[0].IsFarenheit = false;
            }
            else {
                detailPark.Weather[0].IsFarenheit = true;
            }
            return View(detailPark);
        }
        public IActionResult DetailFtoC(ParkInfo park) {
            SetTempCategory(park.TempCategories);
            return RedirectToAction("Detail", "Home", park);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        const string TEMP_SESSION_ID = "Temp_Categories";

        private string GetTempCategory() {
            string category = HttpContext.Session.Get<string>(TEMP_SESSION_ID);

            // See if the user has a shopping cart stored in session            
            if (category == null) {

                // If not, create one for them
                category = "Farenheit";
                HttpContext.Session.Set(TEMP_SESSION_ID, category);
            }
            return category;
        }
        private void SetTempCategory(string temp) {

            HttpContext.Session.Set(TEMP_SESSION_ID, temp);

        }
    }
}
