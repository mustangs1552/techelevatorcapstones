﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Capstone.Web.Models;
using Capstone.Web.DAL;

namespace Capstone.Web.Controllers
{
    public class SurveyController : Controller
    {
        ISurveyResultDAL surveyDAL;
        IParkDAL parkDAL;

        public SurveyController(ISurveyResultDAL surveyDAL, IParkDAL parkDAL)
        {
            this.surveyDAL = surveyDAL;
            this.parkDAL = parkDAL;
        }

        public IActionResult Index()
        {
            SurveyResult surveys = new SurveyResult();
            surveys.VoteResults = new List<SurveyResult>();
            surveys.Results = surveyDAL.GetSurveyResults();

            surveys.SurveyCount = parkDAL.GetTopVotedParksDict();
            /*foreach (SurveyResult survey in surveys.Results)
            {
                surveys.SurveyCount[survey.ParkCode]++;
            }*/
            foreach (KeyValuePair<string, int> survey in surveys.SurveyCount)
            {
                if (survey.Value > 0)
                {
                    surveys.VoteResults.Add(new SurveyResult
                    {
                        ParkCode = survey.Key,
                        State = "",
                        EmailAddress = "",
                        ActivityLevel = "",
                        ImgParkCode = survey.Key
                    });
                }
            }

            return View(surveys);
        }

        [HttpGet]
        public IActionResult NewSurvey()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult NewSurvey(SurveyResult newSurvey)
        {
            try
            {
                /*int surveysExisting = surveyDAL.GetSurveysByEmail(newSurvey.EmailAddress).Count;
                if(surveysExisting > 0)
                {
                    return RedirectToAction("NewSurvey", "Survey");
                }
                else
                {
                    surveyDAL.SaveSurveyResult(newSurvey);
                }*/
                surveyDAL.SaveSurveyResult(newSurvey);
            }
            catch (NullReferenceException)
            {
                throw new Exception();
            }

            return RedirectToAction("Index", "Survey");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
