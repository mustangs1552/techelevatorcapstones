﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models {
    public class WeatherForecast {
        public string ParkCode { get; set; }
        public int DayOfForecast { get; set; }
        public double LowTempF { get; set; }
        public double HighTempF { get; set; }
        public double LowTempC {
            get {
                // (F - 32) * 5/9 = C
                return (LowTempF - 32d) * (5d / 9d);
            }
        }
        public double HighTempC {
            get {
                // (F - 32) * 5/9 = C
                return (HighTempF - 32d) * (5d / 9d);
            }
        }
        public string Forecast { get; set; }
        private string imgforecast = "";
        public string ImgForecast {
            get {
                return imgforecast;
            }
            set {
                imgforecast = value.ToLower() + ".png";
            }
        }
        public string ReccomendationMsg
        {
            get
            {
                string msg = "";

                if(Forecast == "snow")
                {
                    msg += "Pack snowshoes";
                }
                else if(Forecast == "rain")
                {
                    msg += "Pack rain gear and waterproof shoes";
                }
                else if(Forecast == "thunderstorms")
                {
                    msg += "Seek shelter and avoid hiking on exposed ridges";
                }
                else if(Forecast == "sunny")
                {
                    msg += "Pack sunblock";
                    if(HighTempF > 75)
                    {
                        msg += " and bring an extra gallon of water";
                    }
                }

                if(LowTempF < 20)
                {
                    if (msg != "")
                    {
                        msg += ". ";
                    }
                    msg += "Beware of frigid temperatures";
                }
                if(HighTempF - LowTempF > 20)
                {
                    if(msg != "")
                    {
                        msg += ". ";
                    }
                    msg += "Wear breathable layers";
                }

                return msg + ((msg != "") ? "." : "");
            }
        }
        

        public bool IsFarenheit { get; set; }

        public IList<WeatherForecast> Results { get; set; }
    }

}

