﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models {
    public class ParkInfo {
        public string ParkCode { get; set; }
        private string imgParkCode = "";
        public string ImgParkCode {
            get {
                return imgParkCode;
            }
            set {
                imgParkCode = value.ToLower() + ".jpg";
            }
        }
        public string ParkName { get; set; }
        public string State { get; set; }
        public float Acreage { get; set; }
        public float ElevationFeet { get; set; }
        public double TrailMiles { get; set; }
        public float NumberOfCampsites { get; set; }
        public string Climate { get; set; }
        public int YearFounded { get; set; }
        public float AnnualVisitorCount { get; set; }
        public string Quote { get; set; }
        public string QuoteSource { get; set; }
        public string ParkDescription { get; set; }
        public float EntryFee { get; set; }
        public int NumOfAnimalSpecies { get; set; }
        public string TempCategories { get; set; }

        public IList<ParkInfo> Results { get; set; }
        public IList<WeatherForecast> Weather { get; set; }

        public static List<SelectListItem> WeatherTypes = new List<SelectListItem>()
      {
            new SelectListItem() { Text = "Farenheit", Value = "Farenheit" },
            new SelectListItem() { Text = "Celsius", Value = "Celsius" },
            };
    }
}

