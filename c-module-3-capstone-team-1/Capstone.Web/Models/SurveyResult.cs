﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models {
    public class SurveyResult {
        public int SurveyId{ get; set; }
        public string ParkCode { get; set; }
        private string imgParkCode = "";
        public string ImgParkCode {
            get {
                return imgParkCode;
            }
            set {
                imgParkCode = value.ToLower() + ".jpg";
            }
        }
        [Required(ErrorMessage = "*")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "*")]
        public string State { get; set; }
        public string ActivityLevel { get; set; }
        //public int SurveyCount { get; set; }
        public Dictionary<string, int> SurveyCount = new Dictionary<string, int>()
       {
            { "GNP", 0 },
            { "GCNP", 0 },
            { "GTNP", 0 },
            { "MRNP", 0 },
            { "ENP", 0},
            { "YNP",0 },
            { "YNP2", 0},
            { "CVNP",0 },
            { "RMNP",0  },
        };
        public IList<SurveyResult> Results { get; set; }
        public IList<SurveyResult> VoteResults { get; set; }

        public Dictionary<string,string> ParkNames = new Dictionary<string, string>()
       {
            { "GNP", "Glacier National Park" },
            { "GCNP", "Grand Canyon National Park" },
            { "GTNP", "Grand Teton National Park" },
            { "MRNP", "Mount Ranier National Park" },
            { "ENP","Everglades National Park" },
            { "YNP","Yellowstone National Park" },
            { "YNP2","Yosemite National Park"},
            { "CVNP","Cuyahoga Valley National Park" },
            { "RMNP","Rock Mountain National Park"  },
        };
        public static List<SelectListItem> ParkCategories = new List<SelectListItem>()
       {
            new SelectListItem() { Text = "Glacier National Park", Value = "GNP" },
            new SelectListItem() { Text = "Grand Canyon National Park", Value = "GCNP" },
            new SelectListItem() { Text = "Grand Teton National Park", Value = "GTNP" },
            new SelectListItem() { Text = "Mount Ranier National Park", Value = "MRNP" },
            new SelectListItem() { Text = "Everglades National Park", Value = "ENP" },
            new SelectListItem() { Text = "Yellowstone National Park", Value = "YNP" },
            new SelectListItem() { Text = "Yosemite National Park", Value = "YNP2" },
            new SelectListItem() { Text = "Cuyahoga Valley National Park", Value = "CVNP" },
            new SelectListItem() { Text = "Rock Mountain National Park", Value = "RMNP" },
        };
        public static List<SelectListItem> ActivityCategories = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Inactive", Value = "Inactive" },
            new SelectListItem() { Text = "Sedentary", Value = "Sedentary" },
            new SelectListItem() { Text = "Active", Value = "Active" },
            new SelectListItem() { Text = "Extremely Active", Value = "Extremely Active" }
        };
    }
}
