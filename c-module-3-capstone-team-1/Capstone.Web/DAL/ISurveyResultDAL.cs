﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Web.Models;

namespace Capstone.Web.DAL
{
    public interface ISurveyResultDAL
    {
        List<SurveyResult> GetSurveyResults();
        List<SurveyResult> GetSurveyResults(string parkCode = "");
        int GetTotalSurveyResults(string parkCode);
        int SaveSurveyResult(SurveyResult surveyResult);
        List<SurveyResult> GetSurveysByEmail(string email);
    }
}
