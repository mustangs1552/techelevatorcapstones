﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Web.Models;

namespace Capstone.Web.DAL
{
    public interface IParkDAL
    {
        List<ParkInfo> GetAllParks();
        ParkInfo GetPark(string parkCode);
        Dictionary<string, int> GetTopVotedParksDict(int topX = 0);
    }
}
