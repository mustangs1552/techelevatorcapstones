﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Capstone.Web.Models;
using Capstone.Web.DAL;

namespace Capstone.Web.DAL
{
    public class ParkDAL : IParkDAL
    {
        private string connString = "";
        private const string GET_ALL_PARKS_SQL = "SELECT * FROM park";
        private const string GET_PARK_SQL = "SELECT * FROM park WHERE parkCode = @parkCode";
        private const string GET_TOPX_PARKS_SQL = "SELECT TOP @topX survey_result.parkCode, park.parkName, COUNT(survey_result.parkCode) AS voteCount FROM park JOIN survey_result ON park.parkCode = survey_result.parkCode GROUP BY survey_result.parkCode, park.parkName ORDER BY voteCount DESC";
        private const string GET_TOP_PARKS_SQL = "SELECT survey_result.parkCode, park.parkName, COUNT(survey_result.parkCode) AS voteCount FROM park JOIN survey_result ON park.parkCode = survey_result.parkCode GROUP BY survey_result.parkCode, park.parkName ORDER BY voteCount DESC";

        public ParkDAL(string connectionString)
        {
            connString = connectionString;
        }

        private List<ParkInfo> PopulateParkList(SqlDataReader reader)
        {
            List<ParkInfo> output = new List<ParkInfo>();

            while(reader.Read())
            {
                ParkInfo obj = new ParkInfo();
                obj.ParkCode = Convert.ToString(reader["parkCode"]);
                obj.ImgParkCode = obj.ParkCode;
                obj.ParkName = Convert.ToString(reader["parkName"]);
                obj.State = Convert.ToString(reader["state"]);
                obj.Acreage = Convert.ToInt32(reader["acreage"]);
                obj.ElevationFeet = Convert.ToInt32(reader["elevationInFeet"]);
                obj.TrailMiles = Convert.ToDouble(reader["milesOfTrail"]);
                obj.NumberOfCampsites = Convert.ToInt32(reader["numberOfCampsites"]);
                obj.Climate = Convert.ToString(reader["climate"]);
                obj.YearFounded = Convert.ToInt32(reader["yearFounded"]);
                obj.AnnualVisitorCount = Convert.ToInt32(reader["annualVisitorCount"]);
                obj.Quote = Convert.ToString(reader["inspirationalQuote"]);
                obj.QuoteSource = Convert.ToString(reader["inspirationalQuoteSource"]);
                obj.ParkDescription = Convert.ToString(reader["parkDescription"]);
                obj.EntryFee = Convert.ToInt32(reader["entryFee"]);
                obj.NumOfAnimalSpecies = Convert.ToInt32(reader["numberOfAnimalSpecies"]);

                obj.TempCategories = "Farenheit";

                output.Add(obj);
            }

            return output;
        }
        private Dictionary<string, int> PopulateDictionaryOfParkVotes(SqlDataReader reader)
        {
            Dictionary<string, int> output = new Dictionary<string, int>();

            while(reader.Read())
            {
                output.Add(Convert.ToString(reader["parkCode"]), Convert.ToInt32(reader["voteCount"]));
            }

            return output;
        }

        public List<ParkInfo> GetAllParks()
        {
            List<ParkInfo> output = new List<ParkInfo>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(GET_ALL_PARKS_SQL, conn);
                    output = PopulateParkList(cmd.ExecuteReader());
                }
            }
            catch (SqlException)
            {
                throw;
            }

            return output;
        }
        public ParkInfo GetPark(string parkCode)
        {
            List<ParkInfo> output = new List<ParkInfo>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(GET_PARK_SQL, conn);
                    if (parkCode != "")
                    {
                        cmd.Parameters.AddWithValue("@parkCode", parkCode);
                    }
                    output = PopulateParkList(cmd.ExecuteReader());
                }
            }
            catch (SqlException)
            {
                throw;
            }

            return output[0];
        }

        public Dictionary<string, int> GetTopVotedParksDict(int topX = 0)
        {
            Dictionary<string, int> output = new Dictionary<string, int>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand((topX > 0) ? GET_TOPX_PARKS_SQL : GET_TOP_PARKS_SQL, conn);
                    if (topX >= 0)
                    {
                        cmd.Parameters.AddWithValue("@topX", topX);
                    }
                    output = PopulateDictionaryOfParkVotes(cmd.ExecuteReader());
                }
            }
            catch (SqlException)
            {
                throw;
            }

            return output;
        }
    }
}
