﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Capstone.Web.Models;

namespace Capstone.Web.DAL {
    public class SurveyResultDAL : ISurveyResultDAL {
        private string connString = "";
        private const string GET_ALL_SURVEYS = "SELECT * FROM survey_result";
        private const string GET_TOTAL_PARK_SURVEYS = "SELECT * FROM survey_result WHERE parkCode = @parkCode";
        private const string SAVE_NEW_SURVEY_RESULT = "INSERT INTO survey_result VALUES(@parkCode, @email, @state, @activity)";
        private const string GET_SURVAYS_BY_EMAIL = "SELECT * FROM survey_result WHERE emailAddress = @email";

        public SurveyResultDAL(string connectionString) {
            connString = connectionString;
        }

        /// <summary>
        /// Populates and returns a list of survey results by the given reader.
        /// </summary>
        /// <param name="reader">The SqlReader to read from.</param>
        /// <returns>A list of survey results.</returns>
        private List<SurveyResult> PopulateSurveyResultList(SqlDataReader reader) {
            List<SurveyResult> output = new List<SurveyResult>();

            while (reader.Read()) {
                SurveyResult obj = new SurveyResult();
                obj.SurveyId = Convert.ToInt32(reader["surveyId"]);
                obj.ParkCode = Convert.ToString(reader["parkCode"]);
                obj.ImgParkCode = obj.ParkCode;
                obj.EmailAddress = Convert.ToString(reader["emailAddress"]);
                obj.State = Convert.ToString(reader["state"]);
                obj.ActivityLevel = Convert.ToString(reader["activityLevel"]);
                output.Add(obj);
            }

            return output;
        }
        /// <summary>
        /// Checks to see if the given object and its values are valid. Returns true if it is.
        /// </summary>
        /// <param name="surveyResult">The survey result to check.</param>
        /// <returns>True if valid.</returns>
        private bool CheckPopulatedSurveyResult(SurveyResult surveyResult) {
            if (surveyResult == null) {
                return false;
            }
            if (surveyResult.ParkCode == null || surveyResult.ParkCode == "") {
                return false;
            }
            if (surveyResult.EmailAddress == null || surveyResult.EmailAddress == "") {
                return false;
            }
            if (surveyResult.State == null || surveyResult.State == "") {
                return false;
            }
            if (surveyResult.ActivityLevel == null || surveyResult.ActivityLevel == "") {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets all the survey results in the database.
        /// </summary>
        /// <returns>All the survey results in the database.</returns>
        public List<SurveyResult> GetSurveyResults() {
            return GetSurveyResults("");
        }
        /// <summary>
        /// Gets all the survey results in the database or for the given park code if given.
        /// </summary>
        /// <returns>All the survey results in the database or for the given park code if given.</returns>
        public List<SurveyResult> GetSurveyResults(string parkCode = "") {
            List<SurveyResult> output = new List<SurveyResult>();

            try {
                using (SqlConnection conn = new SqlConnection(connString)) {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand((parkCode != "") ? GET_TOTAL_PARK_SURVEYS : GET_ALL_SURVEYS, conn);
                    if (parkCode != "") {
                        cmd.Parameters.AddWithValue("@parkCode", parkCode);
                    }
                    output = PopulateSurveyResultList(cmd.ExecuteReader());
                }
            }
            catch (SqlException) {
                throw;
            }

            return output;
        }

        /// <summary>
        /// Gets the total surveys for the given park code.
        /// </summary>
        /// <param name="parkCode">The psrk code to check.</param>
        /// <returns>The number of surveys for the given park code.</returns>
        public int GetTotalSurveyResults(string parkCode) {
            return GetSurveyResults(parkCode).Count;
        }

        /// <summary>
        /// Adds new survey to the database. Returns number of rows affected (-1 if errored).
        /// </summary>
        /// <param name="surveyResult">The survey to add.</param>
        /// <returns>Returns number of rows affected (-1 if errored).</returns>
        public int SaveSurveyResult(SurveyResult surveyResult) {
            if (!CheckPopulatedSurveyResult(surveyResult)) {
                return -1;
            }

            try {
                using (SqlConnection conn = new SqlConnection(connString)) {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SAVE_NEW_SURVEY_RESULT, conn);
                    cmd.Parameters.AddWithValue("@parkCode", surveyResult.ParkCode);
                    cmd.Parameters.AddWithValue("@email", surveyResult.EmailAddress);
                    cmd.Parameters.AddWithValue("@state", surveyResult.State);
                    cmd.Parameters.AddWithValue("@activity", surveyResult.ActivityLevel);
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException) {
                throw;
            }
        }

        public List<SurveyResult> GetSurveysByEmail(string email)
        {
            List<SurveyResult> output = new List<SurveyResult>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(GET_SURVAYS_BY_EMAIL, conn);
                    cmd.Parameters.AddWithValue("@email", email);
                    output = PopulateSurveyResultList(cmd.ExecuteReader());
                }
            }
            catch (SqlException)
            {
                throw;
            }

            return output;
        }
    }
}
