﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Capstone.Web.Models;

namespace Capstone.Web.DAL
{
    public class WeatherDAL : IWeatherDAL
    {
        private string connString = "";
        private const string GET_ALL_FORECASTS_SQL = "SELECT * FROM weather";
        private const string GET_PARK_FORECAST_SQL = "SELECT * FROM weather WHERE parkCode = @parkCode";

        public WeatherDAL(string connectionString)
        {
            connString = connectionString;
        }

        private List<WeatherForecast> PopulateList(SqlDataReader reader)
        {
            List<WeatherForecast> output = new List<WeatherForecast>();

            while(reader.Read())
            {
                WeatherForecast obj = new WeatherForecast();
                obj.ParkCode = Convert.ToString(reader["parkCode"]);
                obj.DayOfForecast = Convert.ToInt32(reader["fiveDayForecastValue"]);
                obj.LowTempF = Convert.ToDouble(reader["low"]);
                obj.HighTempF = Convert.ToDouble(reader["high"]);
                obj.Forecast = Convert.ToString(reader["forecast"]);
                obj.IsFarenheit = true;
                if (obj.Forecast=="partly cloudy") {

                    obj.ImgForecast = "partlyCloudy";
                }
                else {
                    obj.ImgForecast = obj.Forecast;
                }
                    output.Add(obj);
            }

            return output;
        }

        public List<WeatherForecast> GetAllForecasts()
        {
            return GetParkForecast("");
        }
        public List<WeatherForecast> GetParkForecast(string parkCode = "")
        {
            List<WeatherForecast> output = new List<WeatherForecast>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand((parkCode != "") ? GET_PARK_FORECAST_SQL : GET_ALL_FORECASTS_SQL, conn);
                    if(parkCode != "")
                    {
                        cmd.Parameters.AddWithValue("@parkCode", parkCode);
                    }
                    output = PopulateList(cmd.ExecuteReader());
                }
            }
            catch (SqlException)
            {
                throw;
            }

            return output;
        }
    }
}
