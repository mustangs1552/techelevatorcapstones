﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Transactions;
using Capstone.Web.Models;
using Capstone.Web.DAL;

namespace CapstoneTests
{
    [TestClass]
    public class SurveyResultDALTest
    {
        private const string connString = @"Data Source=.\SQLEXPRESS;Initial Catalog=NPGeek;Integrated Security=true";
        private ISurveyResultDAL surveyDAL = null;
        private TransactionScope trans = null;

        [TestInitialize]
        public void Init()
        {
            surveyDAL = new SurveyResultDAL(connString);

            trans = new TransactionScope();
        }
        [TestCleanup]
        public void CleanUp()
        {
            trans.Dispose();
        }

        [TestMethod]
        public void TestSaveNew()
        {
            SurveyResult newSurvey = new SurveyResult()
            {
                ParkCode = "CVNP",
                EmailAddress = "test@test.com",
                State = "PA",
                ActivityLevel = "Inactive"
            };

            int rowsAffected = surveyDAL.SaveSurveyResult(newSurvey);
            Assert.AreEqual(1, rowsAffected, "Not successfully added!");
        }

        [TestMethod]
        public void TestBlankInput()
        {
            SurveyResult newBlankSurvey = new SurveyResult()
            {
                ParkCode = "",
                EmailAddress = "",
                State = "",
                ActivityLevel = ""
            };
            SurveyResult newNullSurvey = new SurveyResult()
            {
                ParkCode = null,
                EmailAddress = null,
                State = null,
                ActivityLevel = null
            };

            int rowsAffected = surveyDAL.SaveSurveyResult(null);
            Assert.AreEqual(-1, rowsAffected, "Can't add a null survey!");
            rowsAffected = surveyDAL.SaveSurveyResult(newBlankSurvey);
            Assert.AreEqual(-1, rowsAffected, "Can't add a survey with blank values!");
            rowsAffected = surveyDAL.SaveSurveyResult(newNullSurvey);
            Assert.AreEqual(-1, rowsAffected, "Can't add a survey with null values!");
        }
    }
}
