﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Transactions;
using Capstone.Web.Models;
using Capstone.Web.DAL;

namespace CapstoneTests
{
    [TestClass]
    public class WeatherTests
    {
        private const string connString = @"Data Source=.\SQLEXPRESS;Initial Catalog=NPGeek;Integrated Security=true";
        private IWeatherDAL surveyDAL = null;
        private TransactionScope trans = null;

        [TestInitialize]
        public void Init()
        {
            surveyDAL = new WeatherDAL(connString);

            trans = new TransactionScope();
        }
        [TestCleanup]
        public void CleanUp()
        {
            trans.Dispose();
        }

        [TestMethod]
        public void TestFToC()
        {
            WeatherForecast weather = new WeatherForecast()
            {
                LowTempF = 41,
                HighTempF = 50
            };

            Assert.AreEqual(5, weather.LowTempC);
            Assert.AreEqual(10, weather.HighTempC);
        }
    }
}
