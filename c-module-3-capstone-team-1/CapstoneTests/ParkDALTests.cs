using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Capstone.Web.DAL;
using Capstone.Web.Models;
using System.Transactions;

namespace CapstoneTests
{
    [TestClass]
    public class ParkDALTests
    {
        private string connString = @"Data Source=.\SQLEXPRESS;Initial Catalog=NPGeek;Integrated Security=true";
        private IParkDAL parkDAL = null;
        TransactionScope trans = null;

        [TestInitialize]
        public void Init()
        {
            parkDAL = new ParkDAL(connString);

            trans = new TransactionScope();
        }
        [TestCleanup]
        public void CleanUp()
        {
            trans.Dispose();
        }

        [TestMethod]
        public void GetAllParksInitialTests()
        {
            List<ParkInfo> parks = parkDAL.GetAllParks();

            Assert.IsNotNull(parks);
            Assert.AreNotEqual(0, parks.Count);
        }
    }
}
